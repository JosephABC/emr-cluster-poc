# EMR-Cluster-poc

## Description
This repository is for testing out Zeppelin on Yarn deployed using AWS EMR

## Goals
- Prove that driver and executor resources (CPU & Memory) is not dynamically changed with `spark.dynamicAllocation.enabled: true`

## Setup
### Go to EC2 > Network & Security > Key Pairs
![image.png](images/EC2_KeyPair.png)
Key Pair will be automatically downloaded. Save it!

### Go to IAM
1. Go to User Groups > Create Group
    - Create Group with AdministratorAccess
2. Go to Users > Add users
    - Allow Programatic Access
    ![image.png](images/EMR_User.png)
    - Add to Group
    ![image.png](images/EMR_User_2.png)






### Go to AWS EMR
1. Select `Spark: Spark 2.4.8 on Hadoop 2.10.1 YARN and Zeppelin 0.10.0`
![image.png](images/AWS_EMR.png)
2. Select for Security and Access
![image.png](images/AWS_EMR_EC2KeyPair.png)
3. Create Cluster!!!!
4. Allow SSH
    - Click on Security groups for Master link
    ![image.png](images/AWS_EMR_SSH.png)
    - Allow SSH from Anywhere
    ![image.png](images/AWS_EMR_SSH_2.png)
    - Allowed Ports
    ![image.png](images/ports.png)



### Connect to EMR Instance
```
ssh -L 8891:127.0.0.1:8890 -i Test.pem hadoop@ec2-XXX.compute-1.amazonaws.com
```
command `-L` redirectors port `127.0.0.1:8890` of the EMR instance to local port 8891.







## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
